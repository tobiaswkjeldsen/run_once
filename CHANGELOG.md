## 1.0.7+4

- Update comments

## 1.0.7+3

- Update comments
- Update SDK environment

## 1.0.7+2

- Update example

## 1.0.7+1

- Update comments

## 1.0.7

- Update comments

## 1.0.7

- Update comments

## 1.0.6

- Update README; mention the `AsyncMemoizer.runOnce` for `async` functions from `package:async`.

## 1.0.5

- Update README.

## 1.0.4

- Update README.

## 1.0.3

- Minor changes.
- Update README.

## 1.0.2

- Implement some extra awareness around when a function is called; its filename, class and method name and line number.
- Update README.

## 1.0.1

- Update README.

## 1.0.0

- Initial version.
