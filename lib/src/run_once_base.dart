import 'dart:async';

/// [RunOnce] uses [StackTrace.current] and carefully use this to keep record
/// on when a function has been called.
/// It means that the call stack is basically used as a unique identifier
/// for when and where a function has been called.
/// See the [_StackTracer] for the actual usage of [StackTrace.current].
///
/// [RunOnce.runOnce] is "scoped" in the sense it knows what file, class,
/// method and line number in that file, that it was called from, to make sure
/// [RunOnce.runOnce] only runs once in that lexical scope.
///
/// The same methodology applies to [RunOnce.runOnceDestroy] but only
/// the outer lexical scope is part of its "scope"; meaning
/// that [RunOnce.runOnceDestroy] "destroys" i.e. makes [RunOnce.runOnce] able
/// to run once again in whatever lexical scope [RunOnce.runOnceDestroy] was
/// called.
class RunOnce {
  static final RunOnce _instance = RunOnce._internal();

  factory RunOnce() {
    return _instance;
  }

  RunOnce._internal();

  final Map<int, Set<_StackTracer>> _runs = {};

  runOnce(Function function, {Duration? forDuration}) {
    final tracer = _StackTracer(StackTrace.current);

    final fileAndClassNameIdentity = tracer.fileAndClassNameIdentity;

    final tracerCallsRuns = _runs[fileAndClassNameIdentity];

    if (tracerCallsRuns == null) {
      _runs[fileAndClassNameIdentity] = {};
    }

    if (tracerCallsRuns != null && !tracerCallsRuns.contains(tracer)) {
      function();

      tracerCallsRuns.add(tracer);

      if (forDuration != null) {
        Timer(forDuration, () => tracerCallsRuns.remove(tracer));
      }
    }
  }

  destroy() {
    final tracer = _StackTracer(StackTrace.current);

    final fileNameAndClassNameIdentity = tracer.fileAndClassNameIdentity;

    _runs[fileNameAndClassNameIdentity]?.clear();
  }
}

/// The [function] is the function to be called once.
/// Using [forDuration] means for a duration [runOnce]
/// can only run once.
runOnce(Function function, {Duration? forDuration}) {
  RunOnce().runOnce(function, forDuration: forDuration);
}

/// Calling [runOnceDestroy] makes [runOnce] able to run once again.
/// Note that this call is "scoped" based on the lexical scope where
/// [runOnceDestroy] was called.
/// The normal "use case" is to call [runOnceDestroy] somewhere in the same
/// class that called [runOnce].
runOnceDestroy() => RunOnce().destroy();

class _StackTracer {
  final StackTrace _trace;

  late String fileName;
  late String functionName;
  late String callerClassName;
  late String callerFunctionName;
  late int lineNumber;
  late int? columnNumber;

  _StackTracer(this._trace) {
    _parseTrace();
  }

  String _getFunctionNameFromFrame(String frame) {
    // Just giving another nickname to the frame
    var currentTrace = frame;

    // To get rid off the #number thing, get the index of the first whitespace
    var indexOfWhiteSpace = currentTrace.indexOf(' ');

    // Create a substring from the first whitespace index till the end of the string
    var subStr = currentTrace.substring(indexOfWhiteSpace);

    // Grab the function name using reg expr
    var indexOfFunction = subStr.indexOf(RegExp(r'[A-Za-z0-9]'));

    // Create a new substring from the function name index till the end of string
    subStr = subStr.substring(indexOfFunction);

    indexOfWhiteSpace = subStr.indexOf(' ');

    // Create a new substring from start to the first index of a whitespace. This substring gives us the function name
    subStr = subStr.substring(0, indexOfWhiteSpace);

    return subStr;
  }

  void _parseTrace() {
    // The trace comes with multiple lines of strings, (each line is also known as a frame), so split the trace's string by lines to get all the frames
    var frames = _trace.toString().split("\n");

    // The first frame is the current function
    functionName = _getFunctionNameFromFrame(frames[0]);

    // The third frame is the caller function
    callerFunctionName = _getFunctionNameFromFrame(frames[2]);

    /// We separate the class name from the method name by splitting on "."
    /// Index 0 is the class name.
    var classAndMethod = callerFunctionName.split('.');

    callerClassName = classAndMethod[0];

    // The first frame has all the information we need
    var traceString = frames[2];

    // Search through the string and find the index of the file name by looking for the '.dart' regex
    var indexOfFileName = traceString.indexOf(RegExp(r'[A-Za-z_-]+.dart'));

    var fileInfo = traceString.substring(indexOfFileName);

    var listOfInfos = fileInfo.split(":");

    /// Splitting fileInfo by the character ":" separates the file name, the line number and the column counter nicely.
    /// Example: main.dart:5:12
    /// To get the file name, we split with ":" and get the first index
    /// To get the line number, we would have to get the second index
    /// To get the column number, we would have to get the third index
    /// Note; when compiling to machine code the column number is not included.
    fileName = listOfInfos[0];

    var hasColumn = listOfInfos.length == 3;
    if (hasColumn) {
      lineNumber = int.parse(listOfInfos[1]);
      var columnStr = listOfInfos[2];
      columnStr = columnStr.replaceFirst(")", "");
    } else {
      lineNumber = int.parse(listOfInfos[1].replaceFirst(")", ""));
    }
  }

  int get fileAndClassNameIdentity =>
      fileName.hashCode + callerClassName.hashCode;

  @override
  int get hashCode =>
      fileName.hashCode + callerClassName.hashCode + lineNumber.hashCode;

  @override
  bool operator ==(Object other) {
    return other.hashCode == hashCode;
  }

  @override
  String toString() {
    return '$fileName, $callerClassName, $callerFunctionName, $lineNumber';
  }
}
