import 'package:run_once/run_once.dart';
import 'package:test/test.dart';

class TestClass {
  int number = 0;

  something() {
    for (int i = 0; i < 10; i++) {
      /// This will only run once until [dispose] on this
      /// instance is called.
      runOnce(() {
        number++;
      });
    }
  }

  dispose() {
    /// When this is called we clear the [runOnce] for this class alone.
    /// Then [runOnce] can run once again.
    runOnceDestroy();
  }
}

void main() {
  group('runOnce', () {
    setUp(() {
      runOnceDestroy();
    });

    test('Test #1', () {
      var count = 0;

      for (var i = 0; i < 10; i++) {
        runOnce(() {
          count++;
        });
      }

      expect(count, 1);
    });
    test('Test #2', () {
      var count = 0;

      runOnce(() {
        count++;
      });
      runOnce(() {
        count++;
      });

      expect(count, 2);
    });
    test('Test #3', () {
      var count = 0;

      func() {
        runOnce(() {
          count++;
        });
      }

      func();
      func();

      expect(count, 1);
    });
    test('Test #4', () {
      var count = 0;

      func() {
        runOnce(() {
          count++;
        });
      }

      for (var i = 0; i < 10; i++) {
        func();
      }

      expect(count, 1);
    });
    test('Test #5; forDuration', () async {
      var count = 0;

      for (var i = 0; i < 15; i++) {
        await Future.delayed(Duration(milliseconds: 100));

        runOnce(() {
          count++;
        }, forDuration: Duration(milliseconds: 500));
      }

      expect(count, 3);
    });
    test('Test #6; runOnceDestroy', () async {
      final testClass = TestClass();

      testClass.something();

      expect(testClass.number, 1);

      testClass.dispose();

      testClass.something();

      expect(testClass.number, 2);
    });
  });
}
